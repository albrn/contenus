---
title: TPs Python et autre
subtitle: "Tu pourras retrouver ici tous les TPs qui n'utilisent pas le micro:bit.
Tu pourras aborder, en plus du Python, des thèmes comme le réseau, le web ou bien
Unix."
weight: 5
---
