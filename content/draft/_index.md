---
title: Hidden folders
draft: true
---

Nothing in this folder is displayed on the web site (except if the flag
        `--buildDrafts` if passed to the `hugo server` command)

